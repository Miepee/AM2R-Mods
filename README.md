# AM2R-Mods
A repository listing available Mods for AM2R

> **NEITHER I, NOR THE AM2R-COMMUNITY-DEVELOPERS ARE RESPONSIBLE FOR ANY DAMAGE THESE MODS DO TO YOUR PC, USE AT YOUR OWN RISK!!!**   


Table of Contents:
<details>
<summary>[Mods for Speedrunning](#mods-for-speedrunning)</summary>

- [Practice Mod](#practice-mod-by-nommiin)
- [Speedrun Mod](#speedrun-mod-by-miepee-and-metroid3d)
- [Old Speedrun Mod](#old-speedrun-mod-by-metroid3d)
- [Unofficial 1.1.3](#unofficial-113-by-miepee-lojical-banjo)

</details>
<details>
<summary>[Mods for 1.1](#mods-for-11)</summary>

- [AM2R FATE](#am2r-fate-by-saber-zero)
</details>
<details>
<summary>[Mods for Community Updates](#mods-for-community-updates)</summary>

- [AM2R 16:10 Ratio Fix](#am2r-1610-ratio-fix-by-rex109)
- [AM2R Multitroid](#am2r-multitroid-by-milesthenerd)
- [AM2R Multitroid Unofficial Patch](#am2r-multitroid-unofficial-patch-by-dodobirb-milesthenerd)
- [AM2R UnoUno Multitroid](#am2r-unouno-multitroid-by-mimolette-dodobirb-milesthenerd)
- [AM2R Randovania Implementation](#am2r-randovania-implementation-by-the-randovania-team)
- [AM2R AP Integration](#am2r-ap-integration-by-ehseezed-dodobirb)
- [AM2R Re-Splashed](#am2r-re-splashed-by-abyssalcreature)
- [AM2R Multitroid Party Edition](#am2r-multitroid-party-edition-by-dodobirb)
- [AM2R Multitroid Fair Team PVP](#am2r-multitroid-fair-team-pvp-by-dodobirb-milesthenerd)
- [AM2R SA-X Duels](#am2r-sa-x-duels-by-theycallmexander-dodobirb)
- [AM2R The Horde](#am2r-the-horde-by-jesright)
- [AM2R Turbo x Horde](#am2r-turbo-x-horde-by-dodobirb-jesright)
- [AM2R Challenges](#am2r-challenges-by-bastion-b-56)
- [SeedViewer](#seedviewer-by-miepee)
- [Always Fusion-Mode](#always-fusion-mode-by-miepee)
- [AM2R Fusion Sound Pack](#am2r-fusion-sound-pack-by-snakelancer)
- [Another Memetroid 2 Remake](#another-memetroid-2-remake-by-the-metroid)
- [AM2R Pipe Fall SFX](#am2r-pipe-fall-sfx-by-ssanoo)
- [AM2R Turbo Edition](#am2r-turbo-edition-by-dodobirb)
- [AM2R Hellrun](#am2r-hellrun-by-dodobirb)
- [AM2R Floor is Lava](#am2r-floor-is-lava-by-theycallmexander)
- [AM2R Area Randomizer](#am2r-area-randomiser-by-dodobirb)
- [AM2R Area Rando Ultra](#am2r-area-rando-ultra-by-dodobirb)
- [AM2R Godmode](#am2r-godmode-by-dodobirb)
- [AM2R Item Roulette](#am2r-item-roulette-by-mimolette)
- [AM2R SA-X Thoth Stalkers](#am2r-sa-x-thoth-stalkers-by-ssanoo)
- [AM2R Time Trials](#am2r-time-trials-by-maylokana)
- [AM2Retro](#am2retro-by-nero)
- [AM2R Neon Cannon](#am2r-neon-cannon-by-haihaa)
- [AM2R The Pharaoh's Curse](#am2r-the-pharaohs-curse-by-ssanoo)
- [AM2R Octrollber](#am2r-octrollber-by-ssanoo)
- [AM2R PowerSaber](#am2r-powersaber-by-xander)
</details>
<details>
<summary>[Mods for AM2R Demos](#mods-for-am2r-demos-by-doctorm64)</summary>

- [Demo 1.0](#am2r-demo-10)
- [Demo 1.1](#am2r-demo-11)
- [Demo 1.1A](#am2r-demo-11a)
- [Demo 1.2](#am2r-demo-112)
- [Demo 1.2.1](#am2r-demo-121)
- [Demo 1.3](#am2r-demo-13)
- [Demo 1.3.1](#am2r-demo-131)
- [Demo 1.3.2](#am2r-demo-132)
- [Demo 1.3.3](#am2r-demo-133)
- [Demo 1.3.4](#am2r-demo-134)
- [Demo 1.4](#am2r-demo-14)
- [Demo 1.4.1](#am2r-demo-141)
</details>
<details>
<summary>[Mods for archived, older AM2R versions](#mods-for-older-am2r-versions)</summary>

- [AM2R 1.0](#am2r-10-by-doctorm64)
- [AM2R 1.1](#am2r-11-by-doctorm64)
- [AM2R 1.2.1](#am2r-121)
- [AM2R 1.2.2](#am2r-122)
- [AM2R 1.2.3](#am2r-123)
- [AM2R 1.2.4](#am2r-124)
- [AM2R 1.2.5](#am2r-125)
- [AM2R 1.2.6](#am2r-126)
- [AM2R 1.2.7](#am2r-127)
- [AM2R 1.2.8](#am2r-128)
- [AM2R 1.2.9](#am2r-129)
- [AM2R 1.2.9A](#am2r-129a)
- [AM2R 1.2.10](#am2r-1210)
- [AM2R 1.3](#am2r-13)
- [AM2R 1.3.1](#am2r-131)
- [AM2R 1.3.2](#am2r-132)
- [AM2R 1.3.3](#am2r-133)
- [AM2R 1.4](#am2r-14)
- [AM2R 1.4.1](#am2r-141)
- [AM2R 1.4.2](#am2r-142)
- [AM2R 1.4.3](#am2r-143)
- [AM2R 1.5](#am2r-15)
- [AM2R 1.5.1](#am2r-151)
- [AM2R 1.5.2](#am2r-152)
</details>
<details>
<summary>[Mods for newer beta versions of AM2R](#mods-for-newer-beta-by-versions-of-am2r)</summary>

- [AM2R 1.6b1](#am2r-16b1)
- [AM2R 1.6b2](#am2r-16b2)

</details>
<details>
<summary>[Mods for 2.0 April Fools Previews](#mods-for-20-april-fools-previews)</summary>

- [Preview 1](#preview-1)
- [Preview 2](#preview-2)
- [Preview 3](#preview-3)

</details>

# Mods for Speedrunning

## Practice Mod by Nommiin
A mod based on YAL's reconstruction, used for speedrunning practice. Has savestates, in order to easily practice certain Areas or splits.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R_1.1_Practice_Mod.zip?inline=false).  

## Speedrun Mod by Miepee and Metroid3D
A modified of AM2R 1.1 used for speedrunning practice. Has savestates as well as a lot of debug functionality. An extensive list of features can be found in the Profile Notes.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/Speedrun_Mod_AM2R_1.1.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/Speedrun_Mod_AM2R_1.1_lin.zip?inline=false).

## Old Speedrun Mod by Metroid3D
A modified version of AM2R 1.1.2 used for speedrunning practice. Has savestates, all sorts of debug information, quick room warping and much more.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R_1.1_OldM3DSpeedrun_Mod.zip?inline=false).  

## Unofficial 1.1.3 by Miepee, Lojical & Banjo
This is a recreation of Banjo's 1.1.2 mod, designed to be as close to 1.1 from a bytecode perspective. The original 1.1.2 had some code evaluation differences. This mod should circumvent these issues by not using YellowAfterlife's AM2R reconstruction.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R_1_1_3.zip?inline=false).

---

# Mods for 1.1

## AM2R FATE by Saber Zero
A modified version of AM2R 1.1 intended to make you feel overpowered. Hyper Beam is avilable from the start, as well as instantly shinesparking with the `AimLock`-button. This is preconfigured for *only* the third save file.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/FATE%20AM2R.zip?inline=false).

---

# Mods for Community Updates

## AM2R 16:10 Ratio Fix by Rex109
A mod for AM2R to modify the aspect ratio to 16:10, useful for Steam Deck players to prevent stretching/black bars.     
Download links (Win, Lin) can be found [here](https://github.com/Rex109/AM2R-Steam-deck-aspect-ratio-fix/releases/latest).

## AM2R Multitroid by milesthenerd
A modified version of AM2R 1.5.2 that allows you to play co-op with up to 16 people online. More information can be found on milesthenerd's [Repository](https://github.com/lassiterm/AM2R-Multitroid/).  
Download links (Windows, Linux, Android) can be found [here](https://github.com/lassiterm/AM2R-Multitroid/releases/latest).

**Most people play the Unofficial Unofficial patch linked down below, since it fixes a lot of issues of the original and the unnofficial patch.**

## AM2R Multitroid Unofficial patch by DodoBirb, milesthenerd
An unofficial patch that adds more content requested by the community and fixing known issues for milesthenerd's [Multitroid mod](https://github.com/lassiterm/AM2R-Multitroid/), released for AM2R 1.5.5. More information can be found on DodoBirb's 
[Repository](https://github.com/DodoBirby/AM2R-Multitroid-Unofficial-Patch).  
Download links (Win, Lin, Android, Mac) can be found [here](https://github.com/DodoBirby/AM2R-Multitroid-Unofficial-Patch/releases/latest).

**Most people play the Unofficial Unofficial patch linked down below, since it fixes a lot of issues of the original and the unnofficial patch.**

## AM2R UnoUno Multitroid by Mimolette, DodoBirb, milesthenerd
An unofficial patch that adds even more content requested by the community and fixing known issues for DodoBirb's [Multitroid Patch](https://github.com/DodoBirby/AM2R-Multitroid-Unofficial-Patch), released for AM2R 1.5.5. More information can be found on Mimolette's [Repository](https://github.com/VanessaMae1087/AM2R-Multitroid-Unofficial-Unofficial).    
Download links (Win, Lin, Android, Mac) can be found [here](https://github.com/VanessaMae1087/AM2R-Multitroid-Unofficial-Unofficial/releases/latest).

## AM2R Randovania Implementation by The Randovania Team
Traverse SR-388 and its depths, while collecting Metroid DNA in order to fight
the Queen and bring the Baby to the Ship.
The Randovania implementation provides additional robustness and more features compared to the built-in randomizer. 

The Randovania AM2R Page can be found [here](https://randovania.org/games/am2r). For an explanation on what makes it unique from the builtin randomizer, among other frequently asked questions, see [here](https://randovania.org/games/am2r/#am2r-already-has-a-built-in-randomizer-what-does-this-do-differently).

## AM2R AP Integration by Ehseezed, DodoBirb
The goal of this AM2R randomizer implementation is to kill or collect the self specified number of metroids to reach the queen metroid and save the Baby Metroid.
Archipelago provides a generic framework for developing multiworld capability for game randomizers. In all cases, presently, Archipelago is also the randomizer itself.    

The AM2R Archipelago Setup Guide can be found [here](https://github.com/Ehseezed/Archipelago-Integration/releases/latest).      
Download links for the AM2R AP Multiworld (Win, Lin) [here](https://github.com/DodoBirby/AM2R-Multiworld-Mod/releases/latest).

## AM2R Re-Splashed by AbyssalCreature
This mod is a complete reskin of all the visuals in AM2R. It aims to make all the Areas, UI elements, Map, Suits, Enemies, and Projectiles to be more stylized and more friendly to users who play in dimly lit areas.  
Provides individual versions that are built upon Community Updates 1.5.5, Multitroid 1.6.2 or AP Multiworld 1.1.     

The repository for all of these can be found [here](https://github.com/AbyssalCreature/AM2R-Re-Splashed/releases)

## AM2R Multitroid Party Edition by DodoBirb
An unofficial patch that adds more content and fixes issues for milesthenerd's [Multitroid mod](https://github.com/lassiterm/AM2R-Multitroid/), released for AM2R 1.5.5; designed to add several new speedbooster techniques from [Turbo Edition](https://github.com/DodoBirby/AM2R-Turbo-Edition/releases/latest) and other secret stuff for complete and utter chaos.
Client and Server Download (Windows Only) [here](https://github.com/DodoBirby/AM2R-Party-Edition/releases/latest). 

## AM2R Multitroid Fair Team PVP by DodoBirb, milesthenerd
An unofficial patch that adds more content and fixes issues for milesthenerd's [Multitroid mod](https://github.com/lassiterm/AM2R-Multitroid/), released for AM2R 1.5.5. It heavily goes more for the PvP aspect of the game, making it more fair for both teams, and allowing some new mechanics and minigames to it. More information can be found on DodoBirb's [Repository](https://github.com/DodoBirby/AM2R-Multitroid-Fair-Team-PVP).  
Download links (Win, Lin, Android) can be found [here](https://github.com/DodoBirby/AM2R-Multitroid-Fair-Team-PVP/releases/latest).

## AM2R SA-X Duels by TheyCallMeXander, DodoBirb
SA-X Duels is a Re-imagining of the classic Multitroid gamemode "SA-X Mode" that adds new mechanics, map changes, and balance patches to make the experience more fair and competitive for a 1v1 setting!       
Download links (Win, Lin, Mac) can be found [here](https://github.com/TheyCallMeXander/AM2R-SA-X-Duels/releases/tag/1.0.14).

## AM2R The Horde by JesRight
What if Samus was late to her mission on SR-388? Find out in this ruthlessly challenging mod of AM2R. The hornoads are waiting.  
Built upon Multitroid 1.3, the online multiplayer mod by milesthenerd. 
Download links (Win, Lin, Android, Mac) can be found [here](https://github.com/Hornoads/AM2R-The-Horde-Multitroid/releases).

## AM2R Turbo x Horde by DodoBirb, JesRight
What if Samus was late to her mission on SR-388 and needs to speedrun it? Find out on this crossover! The hornoads are waiting.  
A crossover mod that merges JesRight's [Horde mod](https://github.com/Hornoads/AM2R-The-Horde-Multitroid) & DodoBirb's [Turbo mod](https://github.com/DodoBirby/AM2R-Turbo-Edition) into one.  
Download links (Windows, Linux) can be found [here](https://github.com/DodoBirby/Turbo-X-Horde-AM2R/releases/latest).

## AM2R Challenges by Bastion B-56
An AM2R mod that combines many smaller mods to let you mix and match challenges, such as Explodomines, Dread Mode, Total Darkness, Hellrun, etc!        
Download links (Win, Lin) can be found [here](https://github.com/BastionB56/Am2r-Challenges/releases/latest).

## SeedViewer by Miepee
This is a Mod that upon loading a save files puts you into an interactive map where you can see where all the items are located. Consider this like a Spoiler log. Based on 1.5.2, might not be compatible with older or newer seed generators.  
Download for Windows can be found [here](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/SeedViewerRelease.zip).

## Always Fusion-Mode by Miepee  
This mod will have the fusion mode features (aka fusion suit, X and CoreX) always enabled after creating a new save, regardless of difficulty. Old saves are not affected. This means, that you can enjoy the features of Fusion Mode, without its hard difficulty.  
Android builds are included.  
Download for 1.5.2: [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/FusionSuitAlways_win.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/FusionSuitAlways_lin.zip?inline=false).  
Download for 1.5.4: [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/FusionSuitAlways154_win.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/FusionSuitAlways154_lin.zip?inline=false).

## AM2R Fusion Sound Pack by SnakeLancer
A modified version of AM2R 1.5.2 that replaces all of the songs with Fusion related ones. Some are just taken straight out of Fusion, some are remixes.  
Download for Windows can be found [here](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Fusion%20Sound%20Pack.zip?inline=false).

## Another Memetroid 2 Remake by The Metroid
A modified version of AM2R 1.5.2 that introduces memes in text as well as sound. Has some crude humor.   
Download links (Win, Lin, Mac and Android) can be found [here](https://github.com/TheMetroid7998/AnotherMemetroid2Remake/releases/latest).

## AM2R Pipe Fall SFX by ssanoo
Every debris makes the pipe fall sound effect when they bounce. This doesn't really change anything, just the sound effects being replaced. Therefore, this mod is Multitroid compatible. Just use the Multitroid 1.6.2 server to host, as this mod uses the Multitroid version 1.6.2.      
Download links (Win, Lin) can be found [here](https://github.com/Haihaa/AM2R-Pipe-Fall-SFX-Mod/releases/latest).

## AM2R Turbo Edition by DodoBirb
A mod for AM2R 1.5.5 that adds several new speedbooster techniques. Cancel shinesparks into run mid-air with aimlock, keep your speed without mockballing, and store charge by spinjumping into a wall.  
Download link for Windows [here](https://github.com/DodoBirby/AM2R-Turbo-Edition/releases/latest).

## AM2R Hellrun by DodoBirb
AM2R 1.5.5 mod where all rooms outside of save points will deal damage to you. Move quickly to stay alive!  
Download link for Windows [here](https://github.com/DodoBirby/AM2R-Hellrun-Mod/releases/latest).

## AM2R Floor is Lava by TheyCallMeXander
A mod for AM2R 1.5.5 where the floor in SR388 is lava, without any safe points whatsoever, now built in [Unofficial Multitroid](https://github.com/DodoBirby/AM2R-Multitroid-Unofficial-Patch) for Co-op or SA-X experience. Stay off the ground to survive!    
Download link for Windows [here](https://github.com/TheyCallMeXander/FloorIsLavaAM2R/releases/). 

## AM2R Area Randomizer by DodoBirb
A mod for AM2R 1.5.5 that randomizes certain door transitions spread across the map. Based on the Super Metroid Area Rando.  

Download links (Windows, Linux, Android) can be found [here](https://github.com/DodoBirby/AM2R-Area-Rando/releases/latest).

**THIS MOD IS ARCHIVED, IT IS NOT RECOMMENDED TO PLAY THIS VERSION, INSTEAD, PLAY AM2R AREA RANDO ULTRA LISTED BELOW.**

## AM2R Area Rando Ultra by DodoBirb
A mod for AM2R 1.5.5 that randomises certain door transitions spread across the map into two way warps, now built in [Unofficial Multitroid](https://github.com/DodoBirby/AM2R-Multitroid-Unofficial-Patch) for Co-op or SA-X experience. Based on the Super Metroid Area Rando.  
Download link (Win, Android) can be found [here](https://github.com/DodoBirby/AM2R-Area-Rando-Ultra/releases/latest). 

## AM2R Godmode by DodoBirb
A mod for 1.5.5 that gives Samus infinite ammo and infinite health. Have fun being overpowered!  
Download links (Windows Only) [here](https://github.com/DodoBirby/AM2R-Godmode/releases/latest).

## AM2R Item Roulette by Mimolette
This AM2R mod takes the term randomizer... maybe a little too literally. All items rapidly shuffle between eachother, from Missiles, Power Bombs, Screw Attack, you name it. This has some softlock potential, but thats just how the gamble is. Will you get lucky?  
Built upon Multitroid 1.4.1, the [online multiplayer mod](https://github.com/lassiterm/AM2R-Multitroid/) by milesthenerd.  
Download links (Windows Only) [here](https://github.com/VanessaMae1087/AM2R-Item-Roulette/releases/latest).

## AM2R SA-X Thoth Stalkers by ssanoo
Thoth Stalkers is a Re-imagining of the classic Multitroid gamemode "SA-X Mode" that adds new mechanics, map changes, and balance patches focused for a hiding and mistrust game setting.       
Download links (Windows only) [here](https://github.com/Haihaa/Thothstalkers-Updates/releases/latest)
 
## AM2R Time Trials by maylokana
A fork of AM2R Community Updates that replaces the original gameloop of AM2R in favor of a series of movement courses, allowing players to build levels, share them and play other levels. 

Download links (Windows Only) [here](https://github.com/maylokana/AM2R-TimeTrials/releases/latest).  
Download the List of Community Levels [here](https://github.com/VanessaMae1087/AM2R-TimeTrials-Level-List).

## AM2Retro by Nero
A reinvented version of AM2R with content from the past removed in the final game.      
Download links (Win, Lin, Mac) can be found [here](https://github.com/Nero260/AM2Retro/releases/latest)

## AM2R Neon Cannon by ssanoo
A demo for an AM2R mod where you combat void denizens in futuristic SR-388. Can you fight against the Void or will you perish to it?    
**THIS MOD'S DEVELOPMENT HAS BEEN CANCELLED. Will stay here for the sake of archiving.**    
Built upon version 1.5.5 on the Community Updates.

Download links (Windows Only) [here](https://github.com/Haihaa/AM2R-Neon-Cannon/releases/latest)

## AM2R The Pharaohs Curse by ssanoo
This is a Halloween-themed AM2R challenge mod with reskins, a new kind of enemy mechanic, boss changes, and a load of meme energy, released on Halloween 2023, built upon [Unofficial Multitroid](https://github.com/DodoBirby/AM2R-Multitroid-Unofficial-Patch) for Co-op or SA-X experience.  
Download links (Win, Lin) can be found [here](https://github.com/Haihaa/AM2R-Pharaoh-s-Curse/releases/latest)

## AM2R Octrollber by ssanoo
This is a Halloween-themed AM2R mod which is purely visual and changes many sprites to just octroll sprites, released on Halloween 2024.
Download links (Win, Lin) can be found [here](https://github.com/Haihaa/AM2R-Octrollber/releases/latest)

## AM2R Power Saber by TheyCallMeXander
Replaces the beam with a melee strike! Still in alpha, so expect some bugs. Slash Away!        
Download link for Windows [here](https://github.com/TheyCallMeXander/Power-Saber-Mod-for-aM2r/releases/tag/Latest)

---

# Mods for AM2R Demos by DoctorM64
These are mods, so you can launch the respective demos straight from the Launcher without any issues.  

## AM2R Demo 1.0
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.0.zip?inline=false).

## AM2R Demo 1.1
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.1.zip?inline=false).

## AM2R Demo 1.1A
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.1a.zip?inline=false).

## AM2R Demo 1.2
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.2.zip?inline=false).

## AM2R Demo 1.2.1
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.21.zip?inline=false).

## AM2R Demo 1.3
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.3.zip?inline=false).

## AM2R Demo 1.3.1
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.31.zip?inline=false).

## AM2R Demo 1.3.2
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.32.zip?inline=false).

## AM2R Demo 1.3.3
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.33.zip?inline=false).

## AM2R Demo 1.3.4
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.34.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.34Linux.zip?inline=false).

## AM2R Demo 1.4
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.4.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.4Linux.zip?inline=false).

## AM2R Demo 1.4.1
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.41.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.41Linux.zip?inline=false).

---

# Mods for older AM2R versions

## AM2R 1.0 by DoctorM64
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.0.zip?inline=false).

## AM2R 1.1 by DoctorM64
This just is an almost empty mod, so that you can launch AM2R 1.1 from the Launcher without any issues.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R_1.1_by_DoctorM64.zip) and unnofficial [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R_1.1_by_DoctorM64Linux.zip).

## The following Mods are Community Updates from the Community Developers

## AM2R 1.2.1
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.1.zip?inline=false).

## AM2R 1.2.2
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.2.zip?inline=false).

## AM2R 1.2.3
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.3.zip?inline=false).

## AM2R 1.2.4
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.4.zip?inline=false).

## AM2R 1.2.5
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.5.zip?inline=false).

## AM2R 1.2.6
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.6.zip?inline=false).

## AM2R 1.2.7
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.7.zip?inline=false).

## AM2R 1.2.8
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.8.zip?inline=false).

## AM2R 1.2.9
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.9.zip?inline=false).

## AM2R 1.2.9A
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.9A.zip?inline=false).

## AM2R 1.2.10
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.10.zip?inline=false).

## AM2R 1.3
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.3.zip?inline=false).

## AM2R 1.3.1
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.3.1.zip?inline=false).

## AM2R 1.3.2
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.3.2.zip?inline=false).

## AM2R 1.3.3
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.3.3.zip?inline=false).

## AM2R 1.4
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.4.zip?inline=false).

## AM2R 1.4.1
This has the official Android releases included.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.4.1.zip?inline=false).

## AM2R 1.4.2
This has the official Android releases included.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.4.2.zip?inline=false).

## AM2R 1.4.3
This has the official Android releases included.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.4.3.zip?inline=false).

## AM2R 1.5
This has the official Android releases included.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.5.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.5Lin.zip?inline=false).

## AM2R 1.5.1
This has the official Android releases included.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.5.1.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.5.1Lin.zip?inline=false).

## AM2R 1.5.2
This has the official Android releases included.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.5.2.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.5.2Lin.zip?inline=false).

# Mods for newer beta versions of AM2R

## AM2R 1.6b1
First beta of 1.6, built utilizing ProfessorG64.        
Download links (Win, Lin) can be found [here](https://github.com/AM2R-Community-Developers/ProfessorG64/releases/tag/1.6b1)

## AM2R 1.6b2
Second beta of 1.6, built utilizing ProfessorG64.       
Download links (Win, Lin) can be found [here](https://github.com/AM2R-Community-Developers/ProfessorG64/releases/tag/1.6b2)

# Mods for 2.0 April Fools Previews

## Preview 1
A glimpse of what is to come... 
This preview contains the minigame Serradius.    
Download for [Windows](https://github.com/AM2R-Community-Developers/Previews/releases/tag/preview-1)

## Preview 2
A glimpse of what is to come...
This preview contains a remixed map of Metroid: Confrontation.     
Download for [Windows](https://github.com/AM2R-Community-Developers/Previews/releases/tag/preview-2)

## Preview 3
A glimpse of what is to come...
This preview contains AM2R Re-Splashed.     
Download for [Win, Lin, Mac](https://github.com/AM2R-Community-Developers/Previews/releases/tag/preview-3)